import { useEffect, useMemo, useRef, useState } from "react";
import Header from "../components/Header/Header";
import CryptoList from "../components/CryptoList/CryptoList";
import AuthDialog, {
  RefHandlers,
} from "../components/Common/AuthDialog/AuthDialog";
import TradeDialog, { TradeHandlers } from "../components/TradeDialog/TradeDialog";
import Instrument from "../../core/models/instrument.model";

const Home = () => {
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  const loginModal = useRef<RefHandlers>(null);
  const tradeModal = useRef<TradeHandlers>(null);

  useEffect(() => {
    const userToken = sessionStorage.getItem("token");
    setIsLoggedIn(userToken != null);
  }, []);

  const triggerTrade = (instrument: Instrument | null, isAsk: boolean) => {
    console.log("Trigger Trade Called", instrument, isAsk);
    const userToken = sessionStorage.getItem("token");

    if (userToken != null) {
      setIsLoggedIn(true);
      tradeModal.current?.openModal(instrument, isAsk);
      // authorized
    } else {
      setIsLoggedIn(false);
      // not authorized
      loginModal.current?.openModal();
    }
  };

  //method created prevent re-render issue on CryptoItems
  const trade = useMemo(() => {
    return triggerTrade
  }, []);

  const login = () => loginModal.current?.openModal();
  const logout = () => {
    sessionStorage.removeItem("token");
    setIsLoggedIn(false);
  };

  return (
    <>
      <Header
        isLoggedIn={isLoggedIn}
        loginClicked={login}
        logoutClicked={logout}
        tradeClicked={() => trade(null, false)}
      />
      <CryptoList triggerTrade={trade} />

      <AuthDialog isOpenInitial={false} ref={loginModal} loginChanged={setIsLoggedIn} />
      <TradeDialog isOpenInitial={false} ref={tradeModal} />
    </>
  );
};

export default Home;
