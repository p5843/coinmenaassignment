import { Dialog, Transition } from "@headlessui/react";
import {
  Fragment,
  useState,
  forwardRef,
  useImperativeHandle,
  useEffect,
} from "react";
import Instrument from "../../../core/models/instrument.model";
import { assetStore } from "../../../core/store/AppStore";
import Input from "../Common/Input/Input";
import CryptoListSelect from "./CryptoListSelect/CryptoListSelect";

type Props = {
  isOpenInitial: boolean;
};

export type TradeHandlers =
  | {
      openModal: (instrument: Instrument | null, isAsk: boolean) => void;
    }
  | undefined;

const TradeDialog = forwardRef<TradeHandlers, Props>((props, ref) => {
  const allInstruments = assetStore((v) => v.instruments);
  const [isOpen, setIsOpen] = useState(props.isOpenInitial);
  const [instrument, setInstrument] = useState<Instrument | null>(null);
  const [isAsk, setIsAsk] = useState(true);
  const closeModal = () => setIsOpen(false);

  const [cryptoAmount, setCryptoAmount] = useState<number | undefined>(
    undefined
  );
  const [fiatAmount, setFiatAmount] = useState<number | undefined>(undefined);

  useEffect(() => {
    setCryptoAmount(instrument?.priceBtc);
    setFiatAmount(instrument?.priceUsd);
  }, [instrument?.id]);

  const cryptoAmountOnChanged = (input: number | undefined) => {
    setCryptoAmount(input);
    setFiatAmount((instrument!.priceUsd / instrument!.priceBtc) * (input ?? 0));
  };

  const fiatAmountOnChanged = (input: number | undefined) => {
    setFiatAmount(input);
    setCryptoAmount(
      (instrument!.priceBtc / instrument!.priceUsd) * (input ?? 0)
    );
  };

  useImperativeHandle(ref, () => ({
    openModal(instrument: Instrument | null, isAsk: boolean) {
      setInstrument(instrument);
      setIsAsk(isAsk);
      setIsOpen(true);
    },
  }));

  return (
    <Transition appear show={isOpen} as={Fragment}>
      <Dialog
        as="div"
        className="fixed z-10 overflow-y-scroll"
        onClose={closeModal}
      >
        <div className="min-h-screen">
          <Transition.Child
            as={Fragment}
            enter="transition ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="transition ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <Dialog.Overlay className="fixed modal-overlay" />
          </Transition.Child>

          <span
            className="inline-block h-screen align-middle"
            aria-hidden="true"
          >
            &#8203;
          </span>
          <Transition.Child
            as={Fragment}
            enter="transition ease-out duration-300"
            enterFrom="opacity-0 scale-95"
            enterTo="opacity-100 scale-100"
            leave="transition ease-in duration-200"
            leaveFrom="opacity-100 scale-100"
            leaveTo="opacity-0 scale-95"
          >
            <div className="inline-block align-middle modal-container">
              <Dialog.Title as="h3">Trade Screen</Dialog.Title>
              <div>
                {/* <Input
                    name="symbol"
                    label="Symbol"
                    type="text"
                    onChange={(ev) => console.log("change", ev.target.value)}
                    onFocus={(_) => console.log("focus")}
                    onBlur={(_) => console.log("blur")}
                    value={`${instrument?.symbol} - ${instrument?.name}`}
                  /> */}
                <CryptoListSelect
                  instruments={allInstruments}
                  value={instrument}
                  onChange={(v) => setInstrument(v)}
                />
                <Input
                  name="cryptoAmount"
                  label="Crypto Amount (BTC)"
                  type="number"
                  onChange={(ev) =>
                    cryptoAmountOnChanged(parseFloat(ev.target.value))
                  }
                  value={cryptoAmount}
                />
                <Input
                  name="fiatAmount"
                  label="Fiat Amount ($)"
                  type="number"
                  onChange={(ev) =>
                    fiatAmountOnChanged(parseFloat(ev.target.value))
                  }
                  value={fiatAmount}
                />
              </div>

              <div className="text-right">
                <button
                  type="button"
                  className={`inline-flex justify-center w-full ${
                    isAsk ? "ask-button" : "bid-button"
                  }`}
                  onClick={closeModal}
                >
                  {isAsk ? "Ask" : "Bid"}
                </button>
              </div>
            </div>
          </Transition.Child>
        </div>
      </Dialog>
    </Transition>
  );
});

export default TradeDialog;
