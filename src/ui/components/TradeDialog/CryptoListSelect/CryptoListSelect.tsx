import { memo } from "react";
import { Listbox } from '@headlessui/react'
import Instrument from "../../../../core/models/instrument.model";

type Props = {
    instruments: Instrument[],
    value: Instrument | null,
    onChange: (value: Instrument) => void,
}

const CryptoListSelect = (props: Props) => {
  return (
    <Listbox value={props.value} onChange={props.onChange}>
      <Listbox.Button>{props.value?.symbol} - {props.value?.name}</Listbox.Button>
      <Listbox.Options>
        {props.instruments.map((instrument) => (
          <Listbox.Option
            key={instrument.id}
            value={instrument}
          >
            {instrument.symbol} - {instrument.name}
          </Listbox.Option>
        ))}
      </Listbox.Options>
    </Listbox>
  );
};

export default memo(CryptoListSelect);
