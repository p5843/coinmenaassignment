import { Menu, Transition } from "@headlessui/react";
import { Fragment } from "react";
import "./CryptoSorter.css";

type Props = {
  sortNameAsc: React.MouseEventHandler<HTMLButtonElement>;
  sortNameDesc: React.MouseEventHandler<HTMLButtonElement>;
  sortPriceAsc: React.MouseEventHandler<HTMLButtonElement>;
  sortPriceDesc: React.MouseEventHandler<HTMLButtonElement>;
};

const CryptoSorter = (props: Props) => {
  return (
    <Menu as="div" className="relative sort-holder">
      <Menu.Button className="inline-flex justify-center sort-drop-button">
        Sort
      </Menu.Button>

      <Transition
        as={Fragment}
        enter="transition ease-out duration-100"
        enterFrom="opacity-0 scale-95"
        enterTo="opacity-100 scale-100"
        leave="transition ease-in duration-100"
        leaveFrom="opacity-100 scale-100"
        leaveTo="opacity-0 scale-95"
      >
        <Menu.Items className="absolute right-0 sort-drop-holder">
          <Menu.Item>
            <button onClick={props.sortNameAsc}>Sort by name - Asc</button>
          </Menu.Item>
          <Menu.Item>
            <button onClick={props.sortNameDesc}>Sort by name - Desc</button>
          </Menu.Item>
          <Menu.Item>
            <button onClick={props.sortPriceAsc}>Sort by price - Asc</button>
          </Menu.Item>
          <Menu.Item>
            <button onClick={props.sortPriceDesc}>Sort by price - Desc</button>
          </Menu.Item>
        </Menu.Items>
      </Transition>
    </Menu>
  );
};

export default CryptoSorter;
