import { Menu, Transition } from "@headlessui/react";
import { Fragment } from "react";
import "./BidAsk.css";

type Props = {
  bidClicked: React.MouseEventHandler<HTMLButtonElement>;
  askClicked: React.MouseEventHandler<HTMLButtonElement>;
};

const BidAskButtons = (props: Props) => {
  return (
    <Menu as="div" className="relative bidask-holder">
      <Menu.Button className="inline-flex justify-center bidask-drop-button">
        Trade
      </Menu.Button>

      <Transition
        as={Fragment}
        enter="transition ease-out duration-100"
        enterFrom="opacity-0 scale-95"
        enterTo="opacity-100 scale-100"
        leave="transition ease-in duration-100"
        leaveFrom="opacity-100 scale-100"
        leaveTo="opacity-0 scale-95"
      >
        <Menu.Items className="absolute right-0 bidask-drop-holder">
          <Menu.Item>
            <button className="bid-button" onClick={props.bidClicked}>Bid</button>
          </Menu.Item>
          <Menu.Item>
            <button className="ask-button" onClick={props.askClicked}>Ask</button>
          </Menu.Item>
        </Menu.Items>
      </Transition>
    </Menu>
  );
};

export default BidAskButtons;
