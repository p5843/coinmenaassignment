import "./CryptoItem.css";
import Instrument from "../../../../core/models/instrument.model";
import { memo } from "react";
import BidAskButtons from "./BidAsk/BidAsk";

type ItemProps = {
  instrument: Instrument;
  triggerTrade: (instrument: Instrument, isAsk: boolean) => void;
};

const CryptoItem = ({ instrument, triggerTrade }: ItemProps) => {
  return (
    <div className="CryptoItem">
      {console.log(`Item ${instrument.name} reload at ${Date.now()}`)}
      <div className="CryptoItem__icon">
        <img
          src={`https://cryptoicon-api.vercel.app/api/icon/${instrument.symbol.toLocaleLowerCase()}`}
          onError={({ currentTarget }) => {
            currentTarget.onerror = null;
            currentTarget.src =
              "https://upload.wikimedia.org/wikipedia/commons/a/ac/No_image_available.svg";
          }}
          alt={instrument.name}
        />
      </div>
      <div className="CryptoItem__name">{instrument.name}</div>
      <div className="CryptoItem__price">{instrument.priceBtc}</div>

      <BidAskButtons
        bidClicked={() => triggerTrade(instrument, false)}
        askClicked={() => triggerTrade(instrument, true)}
      />
    </div>
  );
};

export default memo(CryptoItem);
