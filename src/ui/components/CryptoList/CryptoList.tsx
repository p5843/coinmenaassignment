import CryptoItem from "./CryptoItem/CryptoItem";
import { assetStore, LoadState } from "../../../core/store/AppStore";
import { useEffect } from "react";
import Loading from "../Common/Loading/Loading";
import Button from "../Common/Button/Button";
import CryptoSorter from "./CryptoSorter/CryptoSorter";
import Instrument from "../../../core/models/instrument.model";

const urlbase = "https://data.messari.io/api/v1/assets?limit=10&page=";

type Props = {
  triggerTrade: (instrument: Instrument, isAsk: boolean) => void,
}

const CryptoList = (props: Props) => {
  const store = assetStore((v) => v);
  const getMore = () => store.fetchInstruments(urlbase + store.fetchedPage);
  const setSortBy = (v: "name" | "priceUsd", asc: boolean) => store.sortInstruments(v, asc);

  useEffect(() => {
    if(store.instruments.length < 10) {
      getMore();
    }
  }, []);

  return (
    <div className="container white-bg App__container">
      {console.log(`crypto list rendered ${new Date()}`)}
      <CryptoSorter
        sortNameAsc={(_) => setSortBy("name", true)}
        sortNameDesc={(_) => setSortBy("name", false)}
        sortPriceAsc={(_) => setSortBy("priceUsd", true)}
        sortPriceDesc={(_) => setSortBy("priceUsd", false)}
        />

      {store.instruments.map((x) => (
        <CryptoItem key={x.id} instrument={x} triggerTrade={props.triggerTrade} />
      ))}
      {store.fetchedStatus === LoadState.Fethcing ? (
        <Loading />
      ) : (
        <Button margin="10px auto" onClick={getMore}>
          LoadMore
        </Button>
      )}
    </div>
  );
};

export default CryptoList;
