import React from "react";
import "./Button.css";

interface Props {
  children?: React.ReactNode;
  onClick?: () => void;
  radius?: string;
  padding?: string;
  margin?: string;
}

const Button : React.FC<Props> = ({ 
    children,
    onClick, 
    radius = "6px",
    padding,
    margin,
  }) => <button 
        style={{
            borderRadius: radius,
            padding,
            margin,
        }}
        className="Button"
        onClick={onClick}>
            {children}
        </button>

  export default Button;