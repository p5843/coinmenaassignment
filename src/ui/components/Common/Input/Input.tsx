import React, { FC, InputHTMLAttributes } from "react";
import "./Input.css";

interface Props extends InputHTMLAttributes<HTMLInputElement> {
  name: string;
  label: string;
}

const Input: FC<Props> = ({ name, label, ...rest }) => {
  return (
    <div className="input-wrapper">
      <label htmlFor={name}>{label}</label>
      <input id={name} {...rest} />
    </div>
  );
};

export default Input;
