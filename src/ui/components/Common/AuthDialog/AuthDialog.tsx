import { Dialog, Transition } from "@headlessui/react";
import { Fragment, useState, forwardRef, useImperativeHandle } from "react";

type Props = {
  isOpenInitial: boolean;
  loginChanged: (value: boolean) => void;
};

export type RefHandlers =
  | {
      openModal: () => void;
    }
  | undefined;

const AuthDialog = forwardRef<RefHandlers, Props>((props, ref) => {
  const [isOpen, setIsOpen] = useState(props.isOpenInitial);
  const closeModal = (returnValue: boolean) => {
    setIsOpen(false);
    sessionStorage.setItem("token", "blah-blah");
    props.loginChanged(returnValue);
  };
  useImperativeHandle(ref, () => ({
    openModal() {
      setIsOpen(true);
    },
  }));

  return (
    <Transition appear show={isOpen} as={Fragment}>
      <Dialog
        as="div"
        className="fixed z-10 overflow-y-scroll"
        onClose={() => closeModal(false)}
      >
        <div className="min-h-screen">
          <Transition.Child
            as={Fragment}
            enter="transition ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="transition ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <Dialog.Overlay className="fixed modal-overlay" />
          </Transition.Child>

          <span
            className="inline-block h-screen align-middle"
            aria-hidden="true"
          >
            &#8203;
          </span>
          <Transition.Child
            as={Fragment}
            enter="transition ease-out duration-300"
            enterFrom="opacity-0 scale-95"
            enterTo="opacity-100 scale-100"
            leave="transition ease-in duration-200"
            leaveFrom="opacity-100 scale-100"
            leaveTo="opacity-0 scale-95"
          >
            <div className="inline-block align-middle modal-container">
              <Dialog.Title as="h3">Login Form</Dialog.Title>
              <div>
                <form>
                  <div>
                    <input
                      name="email"
                      type="email"
                      placeholder="Your e-mail"
                      required
                    />
                  </div>
                  <div>
                    <input
                      name="password"
                      type="password"
                      placeholder="Your password"
                      required
                    />
                  </div>
                </form>
              </div>

              <div className="text-right">
                <button
                  type="button"
                  className="inline-flex justify-center"
                  onClick={() => closeModal(true)}
                >
                  Login
                </button>
              </div>
            </div>
          </Transition.Child>
        </div>
      </Dialog>
    </Transition>
  );
});

export default AuthDialog;
