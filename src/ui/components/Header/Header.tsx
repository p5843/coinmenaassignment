import "./Header.css";

type Props = {
  isLoggedIn: boolean,
  tradeClicked: () => void,
  loginClicked: () => void,
  logoutClicked: () => void,
};

const Header = (props: Props) => {

  return (
    <div className="Header">
      <div className="container header__holder">
        <nav>
          <button>Home</button>
          <button onClick={props.tradeClicked}>Trade</button>
        </nav>
        {props.isLoggedIn ? (
          <button onClick={props.logoutClicked}>Logout</button>
        ) : (
          <button onClick={props.loginClicked}>Login</button>
        )}
      </div>
    </div>
  );
};

export default Header;
