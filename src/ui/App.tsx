import "./App.css";
import Home from "./pages/Home";

const App = () => {
  return (
    <div className="App">
      <Home />
      {console.log(`App reload at ${Date.now()}`)}
    </div>
  );
};

export default App;
