import create from "zustand";
import Instrument from "../models/instrument.model";
import { getDistinctBy, sortBy } from "../utils/array.utils";
import { fetcher } from "../utils/fetch.util";

export enum LoadState {
  Undefined,
  Fethcing,
  Fetched,
  Failed,
}

interface AssetStore {
  instruments: Instrument[];
  fetchedPage: number;
  fetchedStatus: LoadState;
  error: any;
  fetchInstruments: (url: string) => void;
  sortInstruments: (selector: string, isAscending: boolean) => void;
}

export const assetStore = create<AssetStore>((set) => ({
  instruments: [],
  fetchedPage: 1,
  fetchedStatus: LoadState.Undefined,
  error: null,
  fetchInstruments: (url) => _instrumentFetcher(url)(set),
  sortInstruments: (selector, isAscending) =>
    _instrumentSorter(selector, isAscending)(set),
}));

const _instrumentFetcher = (url: string) => (set: Function) => {
  set(() => ({ fetchedStatus: LoadState.Fethcing }));
  fetcher(url)
    .then((v) => {
      const list = v.data.map((x: any) => Instrument.fromApiModel(x));
      set((store: AssetStore) => ({
        fetchedStatus: LoadState.Fetched,
        fetchedPage: store.fetchedPage + 1,
        instruments: getDistinctBy([...store.instruments, ...list], "id"),
      }));
    })
    .catch((err) => {
      set(() => ({ 
        fetchedStatus: LoadState.Failed,
        error: err,
      }));
    });
};

const _instrumentSorter =
  (selector: string, isAscending: boolean) => (set: Function) => {
    set((store: AssetStore) => ({
      instruments: [...sortBy(store.instruments, selector, isAscending)],
    }));
  };

interface TradeStore {
  instrument: Instrument;
  fromAmount: boolean;
}

export const tradeStore = create<TradeStore>((set) => ({
  instrument: Instrument.empty(),
  fromAmount: true,
}));
