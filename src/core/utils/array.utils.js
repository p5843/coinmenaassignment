export const getDistinct = (inputList) => [...inputList.filter((v, i, a) => a.indexOf(v) === i)];
export const getDistinctBy = (inputList, selector) => [...inputList.filter((v, i, a) => a.map(x => x[selector]).indexOf(v[selector]) === i)];
export const sortBy = (inputList, selector, isAscending) => {
    if(typeof inputList[0][selector] === "number") {
        const sortedList = inputList.sort((a,b) =>  a[selector] - b[selector]);
        if(isAscending) {
            return sortedList;
        } else {
            return sortedList.reverse();
        }
    } else {
        const sortedList = inputList.sort((a,b) =>  a[selector] > b[selector] ? 1 : -1);
        if(isAscending) {
            return sortedList;
        } else {
            return sortedList.reverse();
        }
    }    
}