export const fetcher = async (url, headers = {}) => await  fetch(url, headers)
.then(response => response.json())
.then(data => data)
.catch(error => error);