import { useEffect, useState } from "react";
import { fetcher} from "../utils/fetch.util";

export const useFetch = (url: string) => {
  const [data, setData] = useState();
  const [error, setError] = useState();
  const [loading, setLoading] = useState(false);

  useEffect(() => {
      setLoading(true);
      fetcher(url).then(setData).catch(setError).finally(() => setLoading(false));
  }, [url]);

  return {data, error, loading};
};
