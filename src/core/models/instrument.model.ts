export default class Instrument {
  id: string;
  serialId: number;
  symbol: string;
  name: string;
  priceUsd: number;
  priceBtc: number;
  priceEth: number;

  constructor(
    id: string,
    serialId: number,
    symbol: string,
    name: string,
    priceUsd: number,
    priceBtc: number,
    priceEth: number
  ) {
    this.id = id;
    this.serialId = serialId;
    this.symbol = symbol;
    this.name = name;
    this.priceUsd = priceUsd;
    this.priceBtc = priceBtc;
    this.priceEth = priceEth;
  }

  static fromApiModel(map: any) {
    return new Instrument(
      map["id"],
      map["serial_id"],
      map["symbol"],
      map["name"],
      map["metrics"]["market_data"]["price_usd"],
      map["metrics"]["market_data"]["price_btc"],
      map["metrics"]["market_data"]["price_eth"],
    );
  };

  static empty = () => new Instrument("", 0, "", "", 0, 0, 0);

}
